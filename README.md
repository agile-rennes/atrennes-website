Site web Agile Tour Rennes
-

[![pipeline status](https://gitlab.com/agile-rennes/website/badges/master/pipeline.svg)](https://gitlab.com/agile-rennes/website/-/commits/master)

**Développement**

* Pré-requis :

  Installer hugo, trois méthodes :
    - Suivre les intructions ici https://gohugo.io/getting-started/installing/

      OU

    - Installer via le gestionnaire de package du système

      OU

    - Utiliser l'image Docker : `docker compose up -d`

* Exécuter le server (si installé en local) :
  ```
  hugo server
  ```

* L'instance locale est alors accessible ici : http://localhost:1313

  La page se recharge automatiquement à chaque modification du code

* Une fois les modifications faites localement, le déploiement en production se fait au push du code.

**Accès à la page**

Gitlab pages : https://agile-rennes.gitlab.io/atrennes-website/

Url officielle : https://agiletour.agilerennes.org/

**Accès à la page dans les MR**

Cliquer sur "View app" dans la MR

![](mr.png)
