const CUSTOM_THEME_COOKIE_NAME = "at-rennes-custom-theme-2024";
const SESSIONIZE_DIV_ID = "sessionize";
const LIGHT_THEME_CSS = "https://sessionize.com/api/v2/57jle1k2/embedstyle";
const CUSTOM_THEME_CSS = "https://sessionize.com/api/v2/byqvylcd/embedstyle";

document.addEventListener('DOMContentLoaded', function () {
    fetch("json/team.json")
        .then(response => {
            return response.json()
        })
        .then(team => {
            const shuffledTeam = team.sort((a, b) => 0.5 - Math.random())
            const firstLine = document.getElementById('team-first-line')
            const secondLine = document.getElementById('team-second-line')
            if (firstLine !== null) {
                shuffledTeam.slice(0, 4).forEach(member => {
                    displayMemberIn(member, firstLine)
                })
            }
            if (secondLine !== null) {
                shuffledTeam.slice(4, 8).forEach(member => {
                    displayMemberIn(member, secondLine)
                })
            }
        });

    const displayMemberIn = (member, target) => {
        const memberInfos =
            `
                <img src="./images/team/${member.photo}">
                <div>${member.name}</div>
            `
        const memberElement = document.createElement("div")
        memberElement.className = "team-member has-text-centered"
        memberElement.innerHTML = memberInfos
        target.appendChild(memberElement)
    }

    const displayThemeFlag = getCookie(CUSTOM_THEME_COOKIE_NAME)
    if (displayThemeFlag !== null) {
        displayTheme(JSON.parse(displayThemeFlag))
        document.getElementById("theme-switch").checked = JSON.parse(displayThemeFlag)
    }

}, false)

const sessionizeObserver = new MutationObserver(() => {
    if (document.getElementById(SESSIONIZE_DIV_ID)) {
        flagPlatinumSessions();
        sessionizeObserver.disconnect();
    }
});

sessionizeObserver.observe(document, {subtree: true, childList: true});

const switchTheme = (switchButton) => {
    displayTheme(switchButton.checked)
}

const displayTheme = (display) => {
    const theme = document.getElementsByTagName('link')[6];

    const NO_THEME = ""
    const CUSTOM_THEME = "css/theme-2024.css"

    if (display) {
        theme.setAttribute('href', CUSTOM_THEME);
    } else {
        theme.setAttribute('href', NO_THEME);
    }

    setCookie(CUSTOM_THEME_COOKIE_NAME, display)
}

const setCookie = (name, value) => {
    const d = new Date();
    d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

function getCookie(name) {
    const decodedCookie = decodeURIComponent(document.cookie);
    if (decodedCookie === "") {
        return null
    }
    const nameValue = decodedCookie.split(";")
    const cookieData = nameValue.map(value => value.trim().split("="))
    const searchedValue = cookieData.filter(data => data[0] === name)[0][1]
    if (searchedValue === undefined) {
        return null
    }
    return searchedValue
}

function flagPlatinumSessions() {
    const platinumSessions = [
        {
            id: "688939",
            imgSrc: "images/Selection-Premium-Neosoft.png",
            title: "Sélection SII"
        },
        {
            id: "699683",
            imgSrc: "images/Selection-Premium-SII.png",
            title: "Sélection SII"
        }
    ];
    platinumSessions.forEach(session => {
        const sessionElement = document.querySelector("div[data-sessionid='" + session.id + "']")
        const sessionCard = sessionElement.firstElementChild
        const macaronImage = document.createElement("img")
        macaronImage.src = session.imgSrc
        macaronImage.title = session.title

        const hourElement = sessionCard.firstElementChild
        hourElement.classList.add("macaron-premium")
        hourElement.append(macaronImage)
    })

}