const CUSTOM_THEME_COOKIE_NAME = "custom-theme-2023";
const SESSIONIZE_DIV_ID = "sessionize";
const LIGHT_THEME_CSS = "https://sessionize.com/api/v2/57jle1k2/embedstyle";
const CUSTOM_THEME_CSS = "https://sessionize.com/api/v2/byqvylcd/embedstyle";

document.addEventListener('DOMContentLoaded', function () {
    fetch("2023/team.json")
        .then(response => {
            return response.json()
        })
        .then(team => {
            const shuffledTeam = team.sort((a, b) => 0.5 - Math.random())
            const firstLine = document.getElementById('team-first-line')
            const secondLine = document.getElementById('team-second-line')
            if (firstLine !== null) {
                shuffledTeam.slice(0, 4).forEach(member => {
                    displayMemberIn(member, firstLine)
                })
            }
            if (secondLine !== null) {
                shuffledTeam.slice(4, 9).forEach(member => {
                    displayMemberIn(member, secondLine)
                })
            }
        });

    const displayMemberIn = (member, target) => {
        const memberInfos =
            `
                <img src="./images/team/${member.photo}">
                <div>${member.name}</div>
            `
        const memberElement = document.createElement("div")
        memberElement.className = "team-member has-text-centered"
        memberElement.innerHTML = memberInfos
        target.appendChild(memberElement)
    }

    const displayThemeFlag = getCookie(CUSTOM_THEME_COOKIE_NAME)
    if (displayThemeFlag !== null) {
        displayTheme(JSON.parse(displayThemeFlag))
        document.getElementById("theme-switch").checked = JSON.parse(displayThemeFlag)
    }
}, false)

const sessionizeObserver = new MutationObserver(() => {
    if (document.getElementById(SESSIONIZE_DIV_ID)) {
        const displayThemeFlag = getCookie(CUSTOM_THEME_COOKIE_NAME)
        if (displayThemeFlag !== null) {
            updateSessionizeCss(JSON.parse(displayThemeFlag))
        }
        flagPlatinumSessions();
        sessionizeObserver.disconnect();
    }
});

sessionizeObserver.observe(document, {subtree: true, childList: true});

const switchTheme = (switchButton) => {
    displayTheme(switchButton.checked)
}

const displayTheme = (display) => {
    const theme = document.getElementsByTagName('link')[5];

    const LIGHT_THEME = "2023/atrennes.css"
    const CUSTOM_THEME = "2023/atrennes-halloween.css"

    if (display) {
        theme.setAttribute('href', CUSTOM_THEME);
    } else {
        theme.setAttribute('href', LIGHT_THEME);
    }
    updateSessionizeCss(display);

    setCookie(CUSTOM_THEME_COOKIE_NAME, display)
}
const updateSessionizeCss = (displayTheme) => {
    let cssToUse = displayTheme ? CUSTOM_THEME_CSS : LIGHT_THEME_CSS;
    const sessionizeGrid = document.getElementById("sessionize")
    if (sessionizeGrid !== null) {
        const sessionizeStyleSheet = sessionizeGrid.querySelector("link")
        sessionizeStyleSheet.setAttribute("href", cssToUse + "?" + (new Date().getTime()))
    }
}

const setCookie = (name, value) => {
    const d = new Date();
    d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

function getCookie(name) {
    const decodedCookie = decodeURIComponent(document.cookie);
    if (decodedCookie === "") {
        return null
    }
    const nameValue = decodedCookie.split(";")
    if (nameValue.length === 0 || nameValue[0].indexOf(name) === -1) {
        return null
    }
    const splittedNameValue = nameValue[0].split("=")
    if (splittedNameValue.length < 2) {
        return null
    }
    return splittedNameValue[1]
}

function flagPlatinumSessions() {
    const platinumSessions = [
        {
            id: "468285",
            imgSrc: "images/Selection-Platinum-Thales.png",
            title: "Sélection Thales"
        },
        {
            id: "523712",
            imgSrc: "images/Selection-Platinum-ASI.png",
            title: "Sélection ASI"
        }
    ];

    platinumSessions.forEach(session => {
        const sessionElement = document.querySelector("div[data-sessionid='" + session.id + "']")
        const sessionCard = sessionElement.firstElementChild
        const macaronImage = document.createElement("img")
        macaronImage.src = session.imgSrc
        macaronImage.title = session.title

        const hourElement = sessionCard.firstElementChild
        hourElement.classList.add("macaron-platinum")
        hourElement.append(macaronImage)
    })

}